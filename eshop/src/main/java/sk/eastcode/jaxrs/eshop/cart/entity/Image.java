package sk.eastcode.jaxrs.eshop.cart.entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author Martin Petruna
 */
@AllArgsConstructor
@Getter
public class Image {

    private String url;
    private String thumbnailUrl;
    private String catalogUrl;
    
    @JsonCreator
    public static Image createViaDTO(ImageDTO dto) {
       return new Image(dto.url, dto.thumbnailUrl, dto.catalogUrl); 
    }
    
    static class ImageDTO {
        public String url;
        public String thumbnailUrl;
        public String catalogUrl;
    }
}
