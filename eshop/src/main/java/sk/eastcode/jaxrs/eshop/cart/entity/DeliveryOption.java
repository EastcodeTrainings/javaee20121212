package sk.eastcode.jaxrs.eshop.cart.entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author Martin Petruna
 */
@Getter
public class DeliveryOption {

    private String id;
    private Double price;
    private String name;

    @JsonCreator
    public DeliveryOption(@JsonProperty("id") String id, @JsonProperty("price") Double price, 
            @JsonProperty("name") String name) {
        this.id = id;
        this.price = price;
        this.name = name;
    }
    
    
}
