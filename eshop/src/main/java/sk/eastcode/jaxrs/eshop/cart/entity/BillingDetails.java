package sk.eastcode.jaxrs.eshop.cart.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Martin Petruna
 */
@AllArgsConstructor
@Data
@NoArgsConstructor
public class BillingDetails {

    private Address billingAddress;
    private Company company;
}
