package sk.eastcode.jaxrs.eshop.cart.boundary.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import lombok.NoArgsConstructor;
import sk.eastcode.jaxrs.eshop.cart.entity.Cart;

/**
 * @author Martin Petruna
 */
@Data
@NoArgsConstructor
public class CartInfoDTO implements Serializable {

    private Integer count;
    private Double totalPrice;
    private String currency = "EUR";

    public CartInfoDTO(Cart cart) {
        this.count = cart.getItemsCount();
        this.totalPrice = cart.getTotalPrice();
    }
}
