package sk.eastcode.jaxrs.eshop.cart.boundary;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import lombok.NoArgsConstructor;

/**
 * @author Martin Petruna
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddItemRequest implements Serializable {

    private Integer quantity;
    
    private String productId;
}
