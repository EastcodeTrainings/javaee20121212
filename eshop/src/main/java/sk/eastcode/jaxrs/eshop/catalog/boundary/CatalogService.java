package sk.eastcode.jaxrs.eshop.catalog.boundary;

import sk.eastcode.jaxrs.eshop.catalog.entity.Product;
import sk.eastcode.jaxrs.eshop.cart.control.DeliveryOptionRepository;
import sk.eastcode.jaxrs.eshop.cart.control.PaymentMethodRepository;
import sk.eastcode.jaxrs.eshop.catalog.control.ProductRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.Map;

/**
 * @author Martin Petruna
 */
@ApplicationScoped
public class CatalogService {

    @Inject
    PaymentMethodRepository paymentMethodRepository;
    @Inject
    DeliveryOptionRepository deliveryOptionRepository;
    @Inject
    ProductRepository productRepository;

    public List<Product> listProducts() {
        return productRepository.listProducts();
    }

    public List<Product> searchProducts(ProductSearchRequest productSearchRequest) {
        return productRepository.search(productSearchRequest);
    }

    public Map<String, Product> listProductsPerProductId() {
        return productRepository.listProductsAsMap();
    }
}
