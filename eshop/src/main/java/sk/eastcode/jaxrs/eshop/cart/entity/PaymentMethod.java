package sk.eastcode.jaxrs.eshop.cart.entity;

import lombok.Getter;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Martin Petruna
 * @author Patrik Dudits
 */
@Getter
public class PaymentMethod {

    private String id;
    private Double price;
    private String name;

    // @AllArgsConstructor does not work with Jackson
    @JsonCreator
    public PaymentMethod(@JsonProperty("id") String id, @JsonProperty("price") Double price, @JsonProperty("name") String name) {
        this.id = id;
        this.price = price;
        this.name = name;
    }
    
    
}
