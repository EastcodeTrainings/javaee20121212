package sk.eastcode.jaxrs.eshop.cart.boundary.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import lombok.NoArgsConstructor;
import sk.eastcode.jaxrs.eshop.cart.entity.CartItem;
import sk.eastcode.jaxrs.eshop.cart.entity.Image;
import sk.eastcode.jaxrs.eshop.catalog.entity.Product;

/**
 * @author Martin Petruna
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CartItemDTO implements Serializable {

    private String productId;
    private String name;
    private Image image;
    private Integer quantity;
    private Double price;
    private String currency = "EUR";
    private Double totalPrice;
    
    public CartItemDTO(CartItem item, Product product) {
        this.productId = item.getProductId();
        this.name = product.getName();
        this.quantity = item.getQuantity();
        this.price = item.getPrice();
        this.totalPrice = item.getTotalPrice();
        this.image = product.getImage();
        this.name = product.getName();
    }
}
