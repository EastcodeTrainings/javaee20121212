package sk.eastcode.jaxrs.eshop.cart.boundary;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import sk.eastcode.jaxrs.eshop.cart.entity.Cart;
import sk.eastcode.jaxrs.eshop.catalog.boundary.CatalogService;
import sk.eastcode.jaxrs.eshop.cart.boundary.dto.CartDTO;
import sk.eastcode.jaxrs.eshop.cart.boundary.dto.CartInfoDTO;
import sk.eastcode.jaxrs.eshop.cart.boundary.dto.CartOrderDTO;
import sk.eastcode.jaxrs.eshop.infrastructure.auth.AuthenticationToken;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;
import sk.eastcode.jaxrs.eshop.cart.entity.DeliveryOption;
import sk.eastcode.jaxrs.eshop.cart.entity.PaymentMethod;

/**
 * @author Martin Petruna
 */
@Path("/cart")
@RequestScoped
@Produces(MediaType.APPLICATION_JSON)
@Api
public class CartResource {

    @Inject
    CartService cartService;
    @Inject
    CatalogService catalogService;

    @AuthenticationToken
    @PUT
    @ApiOperation("Set parameters of an to-be-ordered cart.")
    public CartDTO modifyCart(CartOrderDTO cartOrderDTO) {
        Cart cart = cartService.updateCart(cartOrderDTO.getDeliveryType(),
                cartOrderDTO.getPaymentMethod(),
                cartOrderDTO.getPerson(),
                cartOrderDTO.getBillingAddress(),
                cartOrderDTO.getAddress());
        return new CartDTO(cart, catalogService.listProductsPerProductId());
    }
    
    @AuthenticationToken
    @POST
    @Path("order")
    @ApiOperation("Confirm order")
    public CartDTO confirmOrder() {
        Cart cart = cartService.processOrder();
        return new CartDTO(cart, catalogService.listProductsPerProductId());
    }

    @AuthenticationToken
    @GET
    @ApiOperation("Reads the contents of cart")
    public CartDTO loadCart() {
        return new CartDTO(cartService.getCart(), catalogService.listProductsPerProductId());
    }

    @AuthenticationToken
    @GET
    @Path("/info")
    @ApiOperation("Gets cart summary info")
    public CartInfoDTO info() {
        return new CartInfoDTO(cartService.getCart());
    }

    @AuthenticationToken
    @GET
    @Path("/delivery")
    @ApiOperation(value="Lists delivery options", responseContainer = "List", response = DeliveryOption.class)
    public List<DeliveryOption> deliveryOptions() {
        return cartService.listDeliveryOptions();
    }
    
    @AuthenticationToken
    @GET
    @Path("/payment")
    @ApiOperation(value="Lists payment options", responseContainer = "List", response = PaymentMethod.class)
    public List<PaymentMethod> paymentOptions() {
        return cartService.listPaymentMethods();
    }

    @AuthenticationToken
    @DELETE
    @Path("/items/{itemId}")
    @ApiOperation("Delete an item from a cart")
    public CartInfoDTO deleteItem(@PathParam("itemId") String itemId) {
        cartService.removeItemFromCart(itemId);
        return info();
    }

    @AuthenticationToken
    @POST
    @Path("/items")
    @ApiOperation("Add an item of a cart. Change quantity of an existing item.")
    public CartInfoDTO addItem(AddItemRequest addItemDTO) {
        cartService.addItemToCart(addItemDTO.getProductId(), addItemDTO.getQuantity());
        return info();
    }
}
