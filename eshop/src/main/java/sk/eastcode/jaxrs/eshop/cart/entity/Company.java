package sk.eastcode.jaxrs.eshop.cart.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Martin Petruna
 */
@AllArgsConstructor
@Data
@NoArgsConstructor
public class Company {

    private String name;
    private String dic;
    private String ico;
    private String icDph;
}
