package sk.eastcode.jaxrs.eshop.catalog.boundary;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author Martin Petruna
 */
@AllArgsConstructor
@Getter
public class ProductSearchRequest {

    private String name;
}
