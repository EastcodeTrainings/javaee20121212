package sk.eastcode.jaxrs.eshop.catalog.entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import sk.eastcode.jaxrs.eshop.cart.entity.Image;

/**
 * @author Martin Petruna
 */
@AllArgsConstructor
@Getter
@ToString
public class Product {

    private String id;
    private String name;
    private Double price;
    private String description;
    private Double rating;
    private Image image;

    @JsonCreator
    public static Product createViaFactoryMethod(
            @JsonProperty("id") String id, 
            @JsonProperty("name") String name,
            @JsonProperty("price") Double price,
            @JsonProperty("description") String description,
            @JsonProperty("rating") Double rating,
            @JsonProperty("image") Image image) {
        return new Product(id, name, price, description, rating, image);
    }

    
}
