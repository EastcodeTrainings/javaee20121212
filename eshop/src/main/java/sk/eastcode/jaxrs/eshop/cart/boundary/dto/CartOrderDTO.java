package sk.eastcode.jaxrs.eshop.cart.boundary.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import lombok.NoArgsConstructor;
import sk.eastcode.jaxrs.eshop.cart.entity.Address;
import sk.eastcode.jaxrs.eshop.cart.entity.BillingDetails;
import sk.eastcode.jaxrs.eshop.cart.entity.Person;

/**
 * @author Martin Petruna
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CartOrderDTO implements Serializable {

    private String deliveryType;    
    private String paymentMethod;
    private Person person;
    private BillingDetails billingAddress;
    private Address address;
}
