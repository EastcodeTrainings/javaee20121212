package sk.eastcode.jaxrs.eshop.cart.boundary.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import static java.util.stream.Collectors.toList;
import lombok.NoArgsConstructor;
import sk.eastcode.jaxrs.eshop.cart.entity.Address;
import sk.eastcode.jaxrs.eshop.cart.entity.BillingDetails;
import sk.eastcode.jaxrs.eshop.cart.entity.Cart;
import sk.eastcode.jaxrs.eshop.cart.entity.Payment;
import sk.eastcode.jaxrs.eshop.cart.entity.Person;
import sk.eastcode.jaxrs.eshop.catalog.entity.Product;

/**
 * @author Martin Petruna
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CartDTO implements Serializable {

    private List<CartItemDTO> cartItems;
    private Payment payment;
    private Person person;
    private BillingDetails billingDetails;
    private Address address;
    
    public CartDTO(Cart cart, Map<String, Product> products) {
        this.payment = cart.getPayment();
        this.person = cart.getPerson();
        this.billingDetails = cart.getBillingDetails();
        this.address = cart.getAddress();
        this.cartItems = cart.getCartItems().stream()
                .map(item -> new CartItemDTO(item, products.get(item.getProductId())))
                .collect(toList());
    }

}
