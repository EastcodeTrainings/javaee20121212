package sk.eastcode.jaxrs.eshop.catalog.boundary;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import sk.eastcode.jaxrs.eshop.catalog.entity.Product;
import sk.eastcode.jaxrs.eshop.infrastructure.auth.AuthenticationToken;

/**
 * @author Martin Petruna
 */
@Path("/catalog")
@RequestScoped
@Produces(MediaType.APPLICATION_JSON)
@Api()
@AuthenticationToken
public class CatalogResource {

    @Inject
    private CatalogService catalogService;

    @GET
    @Path("/homepage")
    @ApiOperation(value = "List all products for homepage", notes = "all products", response = Product.class)
    public List<Product> homepage() {
        return catalogService.listProducts();
    }

    @GET
    @Path("/search")
    @ApiOperation(value = "List all products using name param to do prefix query", notes = "for autocomplete feature",
            response = Product.class)
    public List<Product> search(@QueryParam("q") String name) {
        return catalogService.searchProducts(new ProductSearchRequest(name));
    }
}
