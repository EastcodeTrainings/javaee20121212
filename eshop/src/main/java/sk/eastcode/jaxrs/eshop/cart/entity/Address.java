package sk.eastcode.jaxrs.eshop.cart.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Martin Petruna
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Address {

    private String street;
    private String city;
    private String zip;
    private String country;
}
