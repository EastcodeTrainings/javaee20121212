package sk.eastcode.jaxrs.eshop.cart.control;

import sk.eastcode.jaxrs.eshop.data.DeliveryOptions;
import sk.eastcode.jaxrs.eshop.cart.entity.DeliveryOption;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;
import java.util.Optional;

/**
 * @author Martin Petruna
 */
@ApplicationScoped
public class DeliveryOptionRepository {

    public List<DeliveryOption> listDeliveryOptions() {
        return DeliveryOptions.all();
    }

    public DeliveryOption deliveryOption(String id) {
        Optional<DeliveryOption> first = DeliveryOptions.all().stream().filter(deliveryOption -> deliveryOption.getId().equals(id)).findFirst();
        return first.isPresent() ? first.get() : null;
    }
}
