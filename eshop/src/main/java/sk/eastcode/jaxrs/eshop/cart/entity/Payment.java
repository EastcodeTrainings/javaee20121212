package sk.eastcode.jaxrs.eshop.cart.entity;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.io.IOException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import sk.eastcode.jaxrs.eshop.cart.entity.Payment.PaymentDeserializer;

/**
 * @author Martin Petruna
 */
@Builder
@AllArgsConstructor
@Getter
@ToString
@EqualsAndHashCode
@JsonDeserialize(using = PaymentDeserializer.class)
public class Payment {

    private Double itemsPrice;
    private Double deliveryPrice;
    private String deliveryType;
    private Double transactionPrice;
    private String paymentMethod;
    
    public double getTotalPrice() {
        return itemsPrice 
                + (deliveryPrice != null ? deliveryPrice : 0)
                + (transactionPrice != null ? transactionPrice : 0);
    }
    
    static class PaymentDeserializer extends StdDeserializer<Payment> {
        public PaymentDeserializer() {
            super(Payment.class);
        }
        @Override
        public Payment deserialize(JsonParser jp, DeserializationContext dc) throws IOException, JsonProcessingException {
            ObjectNode n = jp.readValueAsTree();
            return new Payment(n.get("itemsPrice").asDouble(0.0),
                    n.get("deliveryPrice").asDouble(0.0),
                    n.get("deliveryType").asText(null),
                    n.get("transactionPrice").asDouble(0.0),
                    n.get("paymentMethod").asText(null));
        }
        
    }
}
