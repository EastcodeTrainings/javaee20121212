package sk.eastcode.jaxrs.eshop.cart.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Martin Petruna
 */
@AllArgsConstructor
@Data
@NoArgsConstructor
public class Person {

    private String forename;
    private String surname;
    private String email;
    private String phone;

}
