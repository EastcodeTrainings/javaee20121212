package sk.eastcode.jaxrs.eshop.cart.boundary;

import lombok.extern.slf4j.Slf4j;
import sk.eastcode.jaxrs.eshop.cart.entity.Address;
import sk.eastcode.jaxrs.eshop.cart.entity.BillingDetails;
import sk.eastcode.jaxrs.eshop.cart.entity.Cart;
import sk.eastcode.jaxrs.eshop.cart.entity.CartItem;
import sk.eastcode.jaxrs.eshop.cart.entity.DeliveryOption;
import sk.eastcode.jaxrs.eshop.cart.entity.Payment;
import sk.eastcode.jaxrs.eshop.cart.entity.PaymentMethod;
import sk.eastcode.jaxrs.eshop.cart.entity.Person;
import sk.eastcode.jaxrs.eshop.cart.control.DeliveryOptionRepository;
import sk.eastcode.jaxrs.eshop.cart.control.PaymentMethodRepository;
import sk.eastcode.jaxrs.eshop.catalog.control.ProductRepository;
import sk.eastcode.jaxrs.eshop.catalog.control.SessionStorageRepository;
import sk.eastcode.jaxrs.eshop.infrastructure.auth.TokenHolder;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

/**
 * @author Martin Petruna
 */
@Slf4j
@ApplicationScoped
public class CartService {

    @Inject
    TokenHolder tokenHolder;
    @Inject
    SessionStorageRepository sessionStorageRepository;
    @Inject
    ProductRepository productRepository;
    @Inject
    DeliveryOptionRepository deliveryOptionRepository;
    @Inject
    PaymentMethodRepository paymentMethodRepository;

    public List<PaymentMethod> listPaymentMethods() {
        return paymentMethodRepository.listPaymentMethods();
    }

    public List<DeliveryOption> listDeliveryOptions() {
        return deliveryOptionRepository.listDeliveryOptions();
    }

    private void storeCart(Cart cart) {
        sessionStorageRepository.storeCart(getRepositoryKey(), cart);
    }

    private Cart retrieveCart() {
        return sessionStorageRepository.retrieveCart(getRepositoryKey());
    }
    
    private <T,U> Optional<T> patch(T requestedValue, Supplier<T> originalSupplier) {
        return requestedValue != null 
                ? Optional.of(requestedValue)
                : Optional.ofNullable(originalSupplier.get());
    }

    public Cart updateCart(String deliveryType, String paymentMethod, Person person, BillingDetails billingDetails, Address address) {
        Cart cart = sessionStorageRepository.retrieveCart(getRepositoryKey());
        log.debug("Processing order for cart {}.", cart);
        Payment originalPayment = cart.getPayment();
        Optional<String> selectedDeliveryType = patch(deliveryType, originalPayment::getDeliveryType);
        // well, definetely nicer approaches exist for
        Double priceForDelivery = selectedDeliveryType
                .map(t -> deliveryOptionRepository.deliveryOption(t).getPrice())
                .orElse(0.0);
        log.info("Price for delivery {} is {}.", deliveryType, priceForDelivery);
        Double priceForPayment = paymentMethod != null 
                ? paymentMethodRepository.paymentMethod(paymentMethod).getPrice() 
                : originalPayment != null
                ? originalPayment.getTransactionPrice()
                : 0;
        log.info("Price for payment {} is {}.", paymentMethod, priceForPayment);
        Payment payment = new Payment( 
                cart.getItemsPrice(), 
                priceForDelivery, selectedDeliveryType.orElse(null), 
                priceForPayment, paymentMethod);
        cart.setPayment(payment);
        if (person != null) {
            cart.setPerson(person);
        }
        if (billingDetails != null) {
            cart.setBillingDetails(billingDetails);
        }
        if (address != null) {
            cart.setAddress(address);
        }
        log.debug("Cart looks like this {} after initialization.", cart);
        //creating new cart, ordered cart added to history
        return cart;
    }
    
    public Cart processOrder() {
        Cart cart = sessionStorageRepository.retrieveCart(getRepositoryKey());
        if (cart == null || cart.getCartItems().isEmpty()) {
            throw new IllegalStateException("Cannot process order for nothing!");
        }
        log.debug("Processing order for cart {}.", cart);  
        // todo: verify completeness of a cart
        sessionStorageRepository.storeCart(getRepositoryKey(), new Cart());
        sessionStorageRepository.storeHistoryCart(getRepositoryKey(), cart);
        return cart;
    }

    public Cart getCart() {
        return retrieveCart();
    }

    public void removeItemFromCart(String productId) {
        log.debug("About to remove item with productId {} from cart.", productId);
        Cart cart = sessionStorageRepository.retrieveCart(getRepositoryKey());
        if (cart != null) {
            Optional<CartItem> existing = cart.getCartItems().stream().filter(item -> item.getProductId().equals(productId)).findFirst();
            if (existing.isPresent()) {
                log.debug("CartItem for productId {} is already present {}. Will remove it.", productId, existing.get());
                cart.getCartItems().remove(existing.get());
            }
        }
        log.debug("Cart now looks like this {}.", cart);
    }

    public void addItemToCart(String productId, Integer quantity) {
        log.debug("About to add item with productId {} with quantity {} to cart.", productId, quantity);
        Cart cart = sessionStorageRepository.retrieveCart(getRepositoryKey());
        if (cart == null) {
            cart = new Cart();
            storeCart(cart);
        }
        Optional<CartItem> existing = cart.getCartItems().stream().filter(item -> item.getProductId().equals(productId)).findFirst();
        if (existing.isPresent()) {
            log.debug("CartItem for productId {} is already present {}. Will increase quantity by {}.", productId, existing.get(), quantity);
            existing.get().add(quantity);
        } else {
            log.debug("CartItem for productId {} is not present. Will add with quantity {}.", productId, quantity);
            cart.getCartItems().add(new CartItem(productId, quantity, productRepository.listProductsAsMap().get(productId).getPrice()));
        }
        log.debug("Cart now looks like this {}.", cart);
    }

    private String getRepositoryKey(){
        return tokenHolder.getToken();
    }

    public void initCart(){
        storeCart(new Cart());
    }
}
