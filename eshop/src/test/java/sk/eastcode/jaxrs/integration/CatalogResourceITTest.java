package sk.eastcode.jaxrs.integration;

import lombok.extern.slf4j.Slf4j;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.ws.rs.core.HttpHeaders;

import java.net.URI;
import java.util.List;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import javax.ws.rs.core.Response;
import static org.assertj.core.api.Assertions.*;
import org.assertj.core.api.Condition;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.junit.Before;

/**
 * @author Martin Petruna
 */
@Slf4j
@RunWith(Arquillian.class)
public class CatalogResourceITTest {

    @ArquillianResource
    URI uri;

    @Deployment
    public static Archive deployment() throws Exception {
        return JaxRsDeployment.deployment();
    }
    private Client client;
    private WebTarget target;
    
    @Before
    public void initializeClient() {
        this.client = ClientBuilder.newClient();
        this.target = client.target(uri).path("service");
    }
    
    private static Condition<JsonObject> hasField(String field) {
        return new Condition<>((o) -> !o.isNull(field), "has field "+field);
    }
    
    private static Condition<JsonObject> A_PRODUCT = 
        allOf(hasField("id"), hasField("name"), hasField("price"), hasField("rating"), hasField("image"));

    @RunAsClient
    @Test
    public void test_homepage() {
        JsonArray response = target.path("catalog/homepage").request(APPLICATION_JSON).get(JsonArray.class);
        assertThat(response.size()).isGreaterThan(5);
        
        assertThat(response.getValuesAs(JsonObject.class))
                .are(A_PRODUCT);
             
    }
    
     

    @RunAsClient
    @Test
    public void test_search() {
        List<JsonObject> response = search("V");
        assertIsVolvo(response);                

        response = search("Vol");
        assertIsVolvo(response);

        response = search("Volv"); 
        assertIsVolvo(response);
    }

    private static void assertIsVolvo(List<JsonObject> response) {
        assertThat(response).hasSize(1)
                .element(0)
                .is(A_PRODUCT)
                .extracting((v) -> v.getString("name"))
                .containsExactly("Volvo V40");
    }

    private List<JsonObject> search(final String query) {
        JsonArray response = target.path("catalog/search").queryParam("q", query).request(APPLICATION_JSON).get(JsonArray.class);
        System.out.println(query+": "+response);
        return response.getValuesAs(JsonObject.class);
    }

    @RunAsClient
    @Test
    public void test_authentication_token() {

        //without sending token back from client
        Response response = target.path("catalog/homepage").request(APPLICATION_JSON).get();
        String token1 = response.getHeaderString(HttpHeaders.AUTHORIZATION);
        assertThat(token1).isNotNull();

        response = target.path("/catalog/search").queryParam("q", "V").request().get();
        String token2 = response.getHeaderString(HttpHeaders.AUTHORIZATION);
        assertThat(token2).isNotNull();

        assertThat(token1).isNotEqualTo(token2);

        //with sending token back from client
        //without sending token back from client
        response = target.path("/catalog/homepage").request().get();
        token1 = response.getHeaderString(HttpHeaders.AUTHORIZATION);
        assertThat(token1).isNotNull();

        response = target.path("/catalog/search").queryParam("q","V").request()
                .header(HttpHeaders.AUTHORIZATION, token1).get();
        token2 = response.getHeaderString(HttpHeaders.AUTHORIZATION);
        assertThat(token2).isNotNull();

        assertThat(token1).isEqualTo(token2);
    }

    //known issue to me
    @Ignore("CatalogResourceITTest.testSwagger » NoClassDefFound Lorg/openqa/selenium/WebDr...")
    @Test
    public void testSwagger() {
        //browser.navigate().to("http://localhost:8080/swagger.json");
        //assertThat(browser.getPageSource()).contains("List all products for homepage");
    }
}
