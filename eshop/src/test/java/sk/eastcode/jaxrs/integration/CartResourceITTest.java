package sk.eastcode.jaxrs.integration;

import io.restassured.RestAssured;
import static io.restassured.RestAssured.given;
import io.restassured.response.Response;
import lombok.extern.slf4j.Slf4j;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.ws.rs.core.HttpHeaders;
import java.util.Arrays;
import java.util.List;

import java.net.URI;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.junit.Before;
import sk.eastcode.jaxrs.eshop.cart.entity.DeliveryOption;
import sk.eastcode.jaxrs.eshop.cart.entity.PaymentMethod;

/**
 * @author Martin Petruna
 */
@Slf4j
@RunWith(Arquillian.class)
public class CartResourceITTest {

    @ArquillianResource
    private URI uri;
    
    @Deployment
    public static Archive deployment() throws Exception {
        return JaxRsDeployment.deployment();
    }

    @Before
    public void before() {
        RestAssured.baseURI = uri.toString();
        RestAssured.basePath = "/service";
    }

    @RunAsClient
    @Test
    public void test_delivery() {
        Response response = given().when().log().all().get("cart/delivery").prettyPeek().andReturn();
        List<DeliveryOption> deliveryOptionDTOs = Arrays.asList(response.getBody().as(DeliveryOption[].class));
        assertThat(deliveryOptionDTOs.size()).isEqualTo(2);
        String token1 = response.getHeader(HttpHeaders.AUTHORIZATION);
        assertThat(token1).isNotNull();
    }

    @RunAsClient
    @Test
    public void test_payment() {
        Response response = given().when().get("/cart/payment").andReturn(); 
        List<PaymentMethod> paymentMethodDTOs = Arrays.asList(response.getBody().as(PaymentMethod[].class));
        assertThat(paymentMethodDTOs.size()).isEqualTo(2);
        String token1 = response.getHeader(HttpHeaders.AUTHORIZATION);
        assertThat(token1).isNotNull();
    }
}