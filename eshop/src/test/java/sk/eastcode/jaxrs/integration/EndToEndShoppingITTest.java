package sk.eastcode.jaxrs.integration;

import io.restassured.RestAssured;
import static io.restassured.RestAssured.given;
import io.restassured.response.Response;
import java.net.URI;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.data.Offset;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.ws.rs.core.HttpHeaders;
import java.util.Arrays;
import java.util.List;
import javax.ws.rs.core.MediaType;

import static org.assertj.core.api.Assertions.assertThat;
import org.hamcrest.Matchers;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.junit.Before;
import sk.eastcode.jaxrs.eshop.cart.boundary.AddItemRequest;
import sk.eastcode.jaxrs.eshop.cart.boundary.dto.CartDTO;
import sk.eastcode.jaxrs.eshop.cart.boundary.dto.CartInfoDTO;
import sk.eastcode.jaxrs.eshop.cart.boundary.dto.CartItemDTO;
import sk.eastcode.jaxrs.eshop.cart.boundary.dto.CartOrderDTO;
import sk.eastcode.jaxrs.eshop.cart.entity.Address;
import sk.eastcode.jaxrs.eshop.cart.entity.BillingDetails;
import sk.eastcode.jaxrs.eshop.cart.entity.Company;
import sk.eastcode.jaxrs.eshop.cart.entity.DeliveryOption;
import sk.eastcode.jaxrs.eshop.cart.entity.Payment;
import sk.eastcode.jaxrs.eshop.cart.entity.PaymentMethod;
import sk.eastcode.jaxrs.eshop.cart.entity.Person;
import sk.eastcode.jaxrs.eshop.catalog.entity.Product;

@Slf4j
@RunWith(Arquillian.class)
public class EndToEndShoppingITTest {
    @ArquillianResource
    URI baseURI;

    @Deployment
    public static Archive deployment() throws Exception {
        return JaxRsDeployment.deployment();
    }

    @Before
    public void before() {
        RestAssured.baseURI = baseURI.toString();
        RestAssured.basePath = "/service";
    }

    @RunAsClient
    @Test
    public void test_shopping() {

        //1. homepage

        Response response = given().log().all().when().get("/catalog/homepage")
                .then()
                .log()
                .all()
                .statusCode(200)
                .header(HttpHeaders.AUTHORIZATION, Matchers.notNullValue())
                .extract()
                .response();
        String token = response.getHeader(HttpHeaders.AUTHORIZATION);

        Product[] productListDTO = response.getBody().as(Product[].class);
        assertThat(productListDTO).isNotEmpty();

        //2. search

        response = given().log().all().when().header(HttpHeaders.AUTHORIZATION, token)
                .get("/catalog/search?q=V").thenReturn();
        response.then().log().all();
        Product[] productListDTOSearch = response.getBody().as(Product[].class);
        assertThat(productListDTOSearch).hasSize(1);
        String token2 = response.getHeader(HttpHeaders.AUTHORIZATION);
        assertThat(token2).isEqualTo(token);

        //3. add item

        Product volvo = productListDTO[0];
        AddItemRequest addItemDTO = new AddItemRequest(1, volvo.getId());
        response = given().log().all().body(addItemDTO).contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .when().header(HttpHeaders.AUTHORIZATION, token).post("/cart/items")
                .thenReturn();
        response.then().log().all();
        CartInfoDTO cartInfoDTO = response.getBody().as(CartInfoDTO.class);
        assertThat(cartInfoDTO.getCount()).isEqualTo(1);
        assertThat(cartInfoDTO.getCurrency()).isEqualTo("EUR");
        assertThat(cartInfoDTO.getTotalPrice()).isCloseTo(volvo.getPrice(), Offset.offset(0.000001d));
        token2 = response.getHeader(HttpHeaders.AUTHORIZATION);
        assertThat(token2).isEqualTo(token);

        //4. search

        response = given().log().all().when().header(HttpHeaders.AUTHORIZATION, token).get("/catalog/search?q=H").thenReturn();
        response.then().log().all();
        productListDTOSearch = response.getBody().as(Product[].class);
        assertThat(productListDTOSearch.length).isEqualTo(1);
        token2 = response.getHeader(HttpHeaders.AUTHORIZATION);
        assertThat(token2).isEqualTo(token);

        //5. add item

        Product honda = productListDTOSearch[0];
        addItemDTO = new AddItemRequest(1, honda.getId());
        response = given().log().all().body(addItemDTO).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
                .when().header(HttpHeaders.AUTHORIZATION, token).post("/cart/items").thenReturn();
        response.then().log().all();
        cartInfoDTO = response.getBody().as(CartInfoDTO.class);
        assertThat(cartInfoDTO.getCount()).isEqualTo(2);
        assertThat(cartInfoDTO.getCurrency()).isEqualTo("EUR");
        assertThat(cartInfoDTO.getTotalPrice()).isCloseTo(volvo.getPrice() + honda.getPrice(), Offset.offset(0.000001d));
        token2 = response.getHeader(HttpHeaders.AUTHORIZATION);
        assertThat(token2).isEqualTo(token);

        //6. add item

        addItemDTO = new AddItemRequest(2, volvo.getId());
        response = given().log().all().body(addItemDTO).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
                .when().header(HttpHeaders.AUTHORIZATION, token).post("/cart/items").thenReturn();
        response.then().log().all();
        cartInfoDTO = response.getBody().as(CartInfoDTO.class);
        assertThat(cartInfoDTO.getCount()).isEqualTo(4);
        assertThat(cartInfoDTO.getCurrency()).isEqualTo("EUR");
        assertThat(cartInfoDTO.getTotalPrice()).isCloseTo(3 * volvo.getPrice() + honda.getPrice(), Offset.offset(0.000001d));
        token2 = response.getHeader(HttpHeaders.AUTHORIZATION);
        assertThat(token2).isEqualTo(token);

        //7. search

        response = given().log().all().when().header(HttpHeaders.AUTHORIZATION, token).get("/catalog/search?q=M").thenReturn();
        response.then().log().all();
        productListDTOSearch = response.getBody().as(Product[].class);
        assertThat(productListDTOSearch.length).isEqualTo(1);
        token2 = response.getHeader(HttpHeaders.AUTHORIZATION);
        assertThat(token2).isEqualTo(token);

        //8. add item

        Product mitsubishi = productListDTOSearch[0];
        addItemDTO = new AddItemRequest(1, mitsubishi.getId());
        response = given().log().all().body(addItemDTO).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
                .when().header(HttpHeaders.AUTHORIZATION, token).post("/cart/items").thenReturn();
        response.then().log().all();
        cartInfoDTO = response.getBody().as(CartInfoDTO.class);
        assertThat(cartInfoDTO.getCount()).isEqualTo(5);
        assertThat(cartInfoDTO.getCurrency()).isEqualTo("EUR");
        assertThat(cartInfoDTO.getTotalPrice()).isCloseTo(3 * volvo.getPrice() + honda.getPrice() + mitsubishi.getPrice(), Offset.offset(0.000001d));
        token2 = response.getHeader(HttpHeaders.AUTHORIZATION);
        assertThat(token2).isEqualTo(token);

        //9. remove item

        response = given().log().all().when().header(HttpHeaders.AUTHORIZATION, token)
                .delete("/cart/items/{itemsId}", honda.getId()).thenReturn();
        response.then().log().all();
        cartInfoDTO = response.getBody().as(CartInfoDTO.class);
        assertThat(cartInfoDTO.getCount()).isEqualTo(4);
        assertThat(cartInfoDTO.getCurrency()).isEqualTo("EUR");
        assertThat(cartInfoDTO.getTotalPrice()).isCloseTo(3 * volvo.getPrice() + mitsubishi.getPrice(), Offset.offset(0.000001d));
        token2 = response.getHeader(HttpHeaders.AUTHORIZATION);
        assertThat(token2).isEqualTo(token);

        //10. get payment methods

        response = given().log().all().when().header(HttpHeaders.AUTHORIZATION, token).get("/cart/payment").andReturn();
        response.then().log().all();
        List<PaymentMethod> paymentMethodDTOs = Arrays.asList(response.getBody().as(PaymentMethod[].class));
        assertThat(paymentMethodDTOs.size()).isEqualTo(2);
        token2 = response.getHeader(HttpHeaders.AUTHORIZATION);
        assertThat(token2).isEqualTo(token);

        //11. get delivery options

        response = given().log().all().when().header(HttpHeaders.AUTHORIZATION, token).get("/cart/delivery").andReturn();
        response.then().log().all();
        List<DeliveryOption> deliveryOptionDTOs = Arrays.asList(response.getBody().as(DeliveryOption[].class));
        assertThat(deliveryOptionDTOs.size()).isEqualTo(2);
        token2 = response.getHeader(HttpHeaders.AUTHORIZATION);
        assertThat(token2).isEqualTo(token);

        //10. perform order

        String dobierka = paymentMethodDTOs.get(1).getId();
        String kurier = deliveryOptionDTOs.get(0).getId();

        Person personDTO = new Person("Martin", "Petruna", "dabrockij@gmail.com", "0907426133");

        Address addressDTO = new Address("Moldavska 10", "Kosice", "04011", "Slovakia");
        Company companyDTO = new Company("Prodium s.r.o.", "123", "456", "789");

        BillingDetails billingDetailsDTO = new BillingDetails(addressDTO, companyDTO);

        CartOrderDTO cartOrderDTO = new CartOrderDTO(kurier, dobierka, personDTO, billingDetailsDTO, addressDTO);

        response = given().log().all().body(cartOrderDTO).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
                .when().header(HttpHeaders.AUTHORIZATION, token).put("/cart").thenReturn();
        response.then().log().all();

        //11. check full cart

        CartDTO cartDTO = response.getBody().as(CartDTO.class);
        assertThat(cartDTO.getAddress()).isEqualTo(addressDTO);
        assertThat(cartDTO.getBillingDetails()).isEqualTo(billingDetailsDTO);
        assertThat(cartDTO.getPerson()).isEqualTo(personDTO);
        assertThat(cartDTO.getPayment()).isEqualTo(
                Payment.builder()
                        .itemsPrice(3 * volvo.getPrice() + mitsubishi.getPrice())
                        .deliveryType(kurier)
                        .deliveryPrice(deliveryOptionDTOs.get(0).getPrice())
                        .paymentMethod(dobierka)
                        .transactionPrice(paymentMethodDTOs.get(1).getPrice())
                        .build());
        assertThat(cartDTO.getCartItems().size()).isEqualTo(2);
        CartItemDTO volvoItem = cartDTO.getCartItems().get(0);
        CartItemDTO mitsubishiItem = cartDTO.getCartItems().get(1);
        assertThat(volvoItem.getQuantity()).isEqualTo(3);
        assertThat(mitsubishiItem.getQuantity()).isEqualTo(1);
        token2 = response.getHeader(HttpHeaders.AUTHORIZATION);
        assertThat(token2).isEqualTo(token);
        
        // Confirm order
        response = given().log().all().when().header(HttpHeaders.AUTHORIZATION, token).post("/cart/order")
                .then().log().all().extract().response();
        
        // basically the previous assertion should be repeated. But IRL we should return the order now.

        //12. check if new cart has been created

        response = given().log().all().when().header(HttpHeaders.AUTHORIZATION, token).get("/cart/info").andReturn();
        response.then().log().all();
        cartInfoDTO = response.getBody().as(CartInfoDTO.class);
        assertThat(cartInfoDTO.getCount()).isEqualTo(0);
        assertThat(cartInfoDTO.getCurrency()).isEqualTo("EUR");
        assertThat(cartInfoDTO.getTotalPrice()).isCloseTo(0, Offset.offset(0.000001d));
        token2 = response.getHeader(HttpHeaders.AUTHORIZATION);
        assertThat(token2).isEqualTo(token);

        //13. check if previous order is in history

        response = given().log().all().contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .when().header(HttpHeaders.AUTHORIZATION, token).get("/cart").andReturn();
        response.then().log().all();
        cartDTO = response.getBody().as(CartDTO.class);

        //TODO

        //TODO 2 test multiple parallel shoppings

    }
}
