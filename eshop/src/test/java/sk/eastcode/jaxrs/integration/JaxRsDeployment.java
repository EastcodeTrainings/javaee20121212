package sk.eastcode.jaxrs.integration;

import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;

public class JaxRsDeployment {

    public static Archive deployment() throws Exception {
        WebArchive deployment = ShrinkWrap.create(WebArchive.class);
        deployment.addPackages(true, "sk.eastcode.jaxrs.");
        return deployment;
    }
}
