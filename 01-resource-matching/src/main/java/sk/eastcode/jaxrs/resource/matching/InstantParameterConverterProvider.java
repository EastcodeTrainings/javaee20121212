/*
 * Copyright (C) 2016 patrik
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package sk.eastcode.jaxrs.resource.matching;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import javax.ws.rs.ext.ParamConverter;
import javax.ws.rs.ext.ParamConverterProvider;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author patrik
 */
@Provider
public class InstantParameterConverterProvider implements ParamConverterProvider{

    @Override
    public <T> ParamConverter<T> getConverter(Class<T> rawType, 
            Type genericType, Annotation[] annotations) {
        if (Instant.class.isAssignableFrom(rawType)) {
            return (ParamConverter<T>)new ParamConverter<Instant>() {
                @Override
                public Instant fromString(String value) {
                    return DateTimeFormatter.ISO_OFFSET_DATE_TIME
                            .parse(value, Instant::from);
                }

                @Override
                public String toString(Instant value) {
                    return value.toString();
                }
            };
        } else {
            return null;
        }
    }
    
}
