package sk.eastcode.jaxrs.resource.matching;

import java.io.InputStream;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;

/**
 * Task: URI is not a filename.
 * <p>Add method annotations so that image is displayed, and stylesheet is applied.
 * @author patrik
 */
@Path("/index")
public class IndexResource {
    @Context
    ServletContext ctx;
    
    @GET
    public String html() {
        return "<html><head><link rel='stylesheet' type='text/css' href='index'></head>"
                + "<body><img src='index'><h1>Hello</h1></html>";
    }
    
    public String css() {
        return "h1 { color: red;  }";
    }
    
    public InputStream image() {
        return ctx.getResourceAsStream("/microprofile.jpg");
    }
}
