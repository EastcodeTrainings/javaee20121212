/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.eastcode.jaxrs.resource.matching;

import java.io.File;
import java.io.InputStream;
import java.net.URI;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.hamcrest.Matchers;
import static org.hamcrest.Matchers.containsString;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author patrik
 */
@RunWith(Arquillian.class)
public class IndexResourceTest {
    @ArquillianResource
    URI baseUrl;
    
    @Deployment(testable = false)
    public static WebArchive deployment() {
        return ShrinkWrap.create(WebArchive.class)
                .addClass(Rest.class)
                .addClass(IndexResource.class)
                .addAsWebResource(new File("src/main/webapp/microprofile.jpg"));
    }
    
    @Test
    public void testHtml() {
        WebTarget target = target();
        Response resp = target.request(MediaType.TEXT_HTML_TYPE).get();
        assertOk(resp);
        assertThat(resp.readEntity(String.class), containsString("<body>"));
    }

    private void assertOk(Response resp) {
        assertEquals(Response.Status.OK.getStatusCode(), resp.getStatus());
    }

    private WebTarget target() {
        Client c = ClientBuilder.newBuilder().build();
        WebTarget target = c.target(baseUrl).path("rest/index");
        return target;
    }
    
    @Test
    public void testCss() {
        // browser uses type from link tag as its Accept header preference
        Response resp = target().request("text/css").get();
        assertOk(resp);
        assertThat(resp.readEntity(String.class), containsString("color: red"));
    }
    
    @Test
    public void testImage() {
        // browser uses image/* as Accept for <img> tag
        Response resp = target().request("image/*").get();
        assertOk(resp);
        Object entity = resp.getEntity();
        assertTrue("Image is transfered as InputStream", entity instanceof InputStream);
    }
}
