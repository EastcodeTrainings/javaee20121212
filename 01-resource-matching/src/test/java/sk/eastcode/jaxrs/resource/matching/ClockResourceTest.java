/*
 * Copyright (C) 2016 patrik
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package sk.eastcode.jaxrs.resource.matching;

import java.io.File;
import java.net.URI;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author patrik
 */
@RunWith(Arquillian.class)
public class ClockResourceTest {
    @ArquillianResource
    URI baseUrl;
    
    @Deployment(testable = false)
    public static WebArchive deployment() {
        return ShrinkWrap.create(WebArchive.class)
                .addClass(Rest.class)
                .addClass(ClockResource.class)
                .addClass(InstantParameterConverterProvider.class);
    }    
    
    @Test()
    public void testKosice() {
        WebTarget path = ClientBuilder.newClient()
                .target(baseUrl)
                .path("rest")
                .path("clock/kosice");
        Response response = path.matrixParam("time", "2016-12-12T12:00:00Z")
                .request().get();
        final String entity = response.readEntity(String.class);
        System.out.println(entity);
        assertEquals("2016-12-12T13:00+01:00[Europe/Bratislava]", entity);
    }
    
    @Test
    public void testWorldTime() {
        WebTarget path = ClientBuilder.newClient()
                .target(baseUrl)
                .path("rest/clock")
                .path("zone")
                .path("America/New_York");
        Response response = path.matrixParam("time", "2016-12-12T12:00:00Z")
                .request().get();
        final String entity = response.readEntity(String.class);
        System.out.println(entity);
        assertEquals("2016-12-12T07:00-05:00[America/New_York]", entity);
    }
}
